import React from 'react';
import '../styles/index.css'; 

const IndexPage: React.FC = () => {
  return (
    <div className="container mt-5">
      <div className="jumbotron">
        <h1 className="display-4">Bienvenido a Job x</h1>
        <p className="lead">Conexión de Trabajadores Informales con Clientes</p>
        <hr className="my-4" />
        <p>
          En Medellín, el índice de desempleo ha aumentado considerablemente en los últimos años, especialmente en el sector de la economía informal.
          Ante esta problemática, surge la necesidad de crear un proyecto que aborde esta situación y ofrezca una solución innovadora.
        </p>
        <p>
          El objetivo de nuestro proyecto de investigación es desarrollar una aplicación que conecte a los trabajadores informales con sus posibles clientes, y viceversa.
          Esta aplicación se enfocará en el trabajo por obra labor, siguiendo la legislación laboral vigente en Colombia.
        </p>
        <p>
          Nuestra aplicación permitirá a los trabajadores informales ofrecer sus servicios y habilidades a través de perfiles personalizados,
          mientras que los clientes podrán buscar y contratar trabajadores para proyectos específicos.
        </p>
        <p>
          Con esta iniciativa, buscamos contribuir a la reducción del desempleo en Medellín, proporcionando una plataforma que promueva el trabajo digno y formal,
          al tiempo que cumple con los requisitos legales establecidos por las autoridades laborales.
        </p>
      </div>
    </div>
  );
};

export default IndexPage;
