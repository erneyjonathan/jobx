import React from 'react';
import '../styles/login.css';
import Head from 'next/head';
// import img from 'next/image';


const LoginPage: React.FC = () => {
  return (
    <div className="container-fluid">
      <Head>
        <title>Login</title>
      </Head>
      <header>
        <div className="flex">
          <div className="iz"></div>
          <div className="mid">
            <center>
          <h1 className='center'>  LOGO </h1>
          </center>
          </div>
          <div className="der"></div>
        </div>
      </header>
      <div className="todo">
        <div className="container mt-12">
          <div  className="row justify-content-center align-items-center h-100">
            <div id='mg' className="col-md-4">
              <div id='caard2' className="card p-4">
                <h2 id='login'>Ingresa a tu cuenta</h2>
                <hr />
                <form>
                  <div className="mb-3">
                    <br />
                    <input placeholder="Usuario" type="email" className="form-control" id="email" aria-describedby="emailHelp" />
                  </div>
                  <br />
                  <div className="mb-3">
                    <input placeholder='Contraseña' type="password" className="form-control" id="password" />
                  </div>
                  <br />
                  <a href="#" className='a'><h2 id='login2'>Olvidé mi contraseña</h2></a>
                  <br />
                  <center>
                    <button id='boton' type="submit" className="btn btn-primary">Ingresar</button>
                  </center>
                </form>
              </div>
            </div>
            <div className="col-md-4">
              <div id='caard22' className="card p-4">

                <h5 >¿Aun no tienes una cuenta?</h5><br />
                <div>
                </div>
                <p>¡Únete ahora a JobXpress para acceder a oportunidades de trabajo o encontrar los servicios que necesitas! Es fácil y rápido comenzar en nuestra plataforma de trabajos informales</p>
                <hr />
                <div className='flex'>
                  <button id='boton2' type="submit" className="btn btn-primary">Trabajador</button>
                  <button id='boton3' type="submit" className="btn btn-primary">Cliente</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer>
      </footer>
    </div>
  );
};

export default LoginPage;