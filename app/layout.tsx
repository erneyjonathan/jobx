import Head from 'next/head';

const Layout: React.FC = ({ children }) => {
  return (
    <html lang="es">
      <Head>
        <title>Título de tu página</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <body className="d-flex flex-column h-100"> 
        <div className="container flex-grow-1">
          <header>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
              <div className="container">
                <a className="navbar-brand" href="#">Mi Aplicación</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <a className="nav-link" href="/">Inicio</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="/about">Acerca de</a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
          </header>
    
          <main>{children}</main>
        </div>
    
        <footer className="footer bg-dark text-light text-center py-4">
          <div className="container">
            &copy; 2024 Mi Empresa
          </div>
        </footer>
      </body>
    </html>
  );
};

export default Layout;
